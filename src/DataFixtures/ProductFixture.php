<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 1; $i <= 20; $i++) {
            $product = new Product();
            $product->setName("Product $i");
            $product->setPrice($this->makePrice());
            $manager->persist($product);
        }
        $manager->flush();
    }

    private function makePrice(): float
    {
        $randomFloat = (mt_rand(7000, 20000) / 100);
        return number_format($randomFloat, 2, '.', '');
    }
}
