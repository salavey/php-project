<?php

namespace App\DataFixtures;

use App\Entity\Loyalty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LoyaltyFixture extends Fixture
{
    const LOYALTY = [
        [
            'title' => 'Новичок',
            'description' => 'Вы только начинаете свой путь в мире гастрономических удовольствий. Но не волнуйтесь, впереди много интересного!',
            'amountMin' => 0,
            'amountMax' => 299,
            'discount' => 5,
        ],
        [
            'title' => 'Едок',
            'description' => 'Вы уже не новичок и с удовольствием пробуете новые блюда. Ваш аппетит растет, и это здорово!',
            'amountMin' => 300,
            'amountMax' => 599,
            'discount' => 10,
        ],
        [
            'title' => 'Лакомка',
            'description' => 'Вы настоящий ценитель вкусной еды! Вы знаете толк в кулинарии и не упускаете возможности насладиться изысканными блюдами.',
            'amountMin' => 600,
            'amountMax' => 999,
            'discount' => 20,
        ],
        [
            'title' => 'Гурман',
            'description' => 'Вы истинный гурман! Для вас еда – это не просто удовлетворение потребности, а настоящее искусство, которое вы цените и наслаждаетесь каждым кусочком.',
            'amountMin' => 1000,
            'amountMax' => 10000,
            'discount' => 30,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach(self::LOYALTY as $loy) {
            $loyalty = new Loyalty();
            $loyalty->setTitle($loy['title'])
                ->setDescription($loy['description'])
                ->setAmountMin($loy['amountMin'])
                ->setAmountMax($loy['amountMax'])
                ->setDiscount($loy['discount']);
            $manager->persist($loyalty);
        }
        $manager->flush();
    }
}
