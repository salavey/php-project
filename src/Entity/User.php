<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: "`user`")]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column]
    private int $chat;

    #[ORM\ManyToOne(targetEntity: Loyalty::class, inversedBy: "users")]
    private ?Loyalty $loyalty = null;

    #[ORM\OneToMany(targetEntity: Cart::class, mappedBy: "user")]
    private Collection $cartItems;

    #[ORM\OneToMany(targetEntity: Order::class, mappedBy: "user")]
    private ?Collection $orders;

    public function __construct()
    {
        $this->cartItems = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLoyalty(): ?Loyalty
    {
        return $this->loyalty;
    }

    public function setLoyalty(?Loyalty $loyalty): self
    {
        $this->loyalty = $loyalty;
        return $this;
    }

    public function getChat(): int
    {
        return $this->chat;
    }

    public function setChat(int $chatId): self
    {
        $this->chat = $chatId;
        return $this;
    }

    public function getCartItems(): Collection
    {
        return $this->cartItems;
    }

    public function setCartItems(Collection $cartItems): self
    {
        $this->cartItems = $cartItems;
        return $this;
    }

    public function __toString(): string
    {
        return sprintf(
            "<b>ID пользователя:</b> %d" . PHP_EOL
            . "<b>Chat ID:</b> %d" . PHP_EOL
            . "<b>Программа лояльности:</b> %d" . PHP_EOL
            . '-------------------------------------------------------------------'. PHP_EOL,
            $this->getId(),
            $this->getChat(),
            $this->getLoyalty()->getTitle()
        );
    }
}
