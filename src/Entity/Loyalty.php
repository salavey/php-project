<?php

namespace App\Entity;

use App\Repository\LoyaltyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity(repositoryClass: LoyaltyRepository::class)]
#[ORM\Table(name: "`loyalty`")]
class Loyalty
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column]
    private string $title;

    #[ORM\Column(length: 1024)]
    private string $description;

    #[ORM\Column]
    private int $amountMin;

    #[ORM\Column]
    private int $amountMax;

    #[ORM\Column]
    private int $discount;

    #[ORM\OneToMany(targetEntity: User::class, mappedBy: "loyalty")]
    private ?Collection $users = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getAmountMin(): int
    {
        return $this->amountMin;
    }

    public function setAmountMin(int $amountMin): self
    {
        $this->amountMin = $amountMin;
        return $this;
    }

    public function getAmountMax(): int
    {
        return $this->amountMax;
    }

    public function setAmountMax(int $amountMax): self
    {
        $this->amountMax = $amountMax;
        return $this;
    }

    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getUsers(): ?Collection
    {
        return $this->users;
    }

    public function setUsers(?Collection $users): self
    {
        $this->users = $users;
        return $this;
    }

    public function __toString(): string
    {
        return sprintf(
            "<b>%s</b>" . PHP_EOL
            . "%s" . PHP_EOL
            . "Сумма Min: %d" . PHP_EOL
            . "Сумма Max: %d" . PHP_EOL
            . "Скидка: %d%%" . PHP_EOL
            . '-------------------------------------------------------------------'. PHP_EOL,
            $this->getTitle(),
            $this->getDescription(),
            $this->getAmountMin(),
            $this->getAmountMax(),
            $this->getDiscount(),
        );
    }
}
