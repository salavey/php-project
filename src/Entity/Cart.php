<?php

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CartRepository::class)]
#[ORM\Table(name: "`cart`")]
class Cart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: 'datetime', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'integer')]
    private int $quantity;

    #[ORM\Column(type: "decimal", precision: 10, scale: 2)]
    private float $price;

    #[ORM\Column(type: "decimal", precision: 10, scale: 2)]
    private float $priceDiscount;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: "cartItems")]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'cartItems')]
    #[ORM\JoinColumn(nullable: false)]
    private Product $product;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'cartItems')]
    #[ORM\JoinColumn(nullable: true)]
    private Order $order;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getPriceDiscount(): float
    {
        return $this->priceDiscount;
    }

    public function setPriceDiscount(float $priceDiscount): self
    {
        $this->priceDiscount = $priceDiscount;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function __toString(): string
    {
        $output = sprintf('%s - %d шт.',
            $this->getProduct()->getName(),
            $this->getQuantity()
        );
        if ($this->getPriceDiscount() < $this->getPrice()) {
            $output .= sprintf(', <del>%s руб.</del> %s руб.',
                $this->getPrice(),
                $this->getPriceDiscount()
            );
        } else {
            $output .= sprintf(', %s руб.', $this->getPrice());
        }
        $output .= PHP_EOL;
        return $output;
    }
}
