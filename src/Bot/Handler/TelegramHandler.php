<?php

namespace App\Bot\Handler;

use App\Bot\Client\ClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class TelegramHandler implements BotHandlerInterface
{
    private Client $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client->getClient();
    }
    public function getMessages(?int $lastUpdateId): ResponseInterface
    {
        try {
            return $this->client->get('getUpdates', [
                'query' => [
                    'offset' => $lastUpdateId,
                ],
            ]);
        } catch (GuzzleException $e) {
            new \Exception($e->getMessage());
        }
    }

    public function getContents(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true)['result'];
    }

    public function sendMessage(int $chatId, string $message): void
    {
        $this->client->post('sendMessage', [
            'form_params' => [
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => 'HTML',
            ],
        ]);
    }
}
