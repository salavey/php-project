<?php

namespace App\Bot\Handler;

use Psr\Http\Message\ResponseInterface;

interface BotHandlerInterface
{
    public function getMessages(int $lastUpdateId): ResponseInterface;

    public function sendMessage(int $chatId, string $message): void;
}
