<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;
use App\Entity\Loyalty;
use App\Entity\User;
use App\Repository\LoyaltyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class StatusTelegramCommand implements CommandInterface
{
    private LoyaltyRepository $loyaltyRepository;
    private UserRepository $userRepository;
    public function __construct(private BotHandlerInterface $botHandler,
                                private int                 $chatId,
                                private EntityManagerInterface $entityManager)
    {
        $this->loyaltyRepository = $this->entityManager->getRepository(Loyalty::class);
        $this->userRepository = $this->entityManager->getRepository(User::class);
    }
    public function run(): void
    {
        $this->botHandler->sendMessage(
            $this->chatId,
            'Твой статус в программе лояльности:' . PHP_EOL . '<b>' . $this->getStatus() . '</b>'
        );
    }

    public function getStatus(): ?string
    {
        $user = $this->userRepository->getUserByChatId($this->chatId);
        return (!empty($user->getLoyalty()))
            ? $user->getLoyalty()->getTitle() . ' (скидка: ' . $user->getLoyalty()->getDiscount() . '%)'
            : 'не зарегестрирован';
    }
}
