<?php

namespace App\Bot\Command;

interface CommandInterface
{
    public function run(): void;
}
