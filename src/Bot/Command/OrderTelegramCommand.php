<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;
use App\Entity\Cart;
use App\Entity\Loyalty;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\LoyaltyRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class OrderTelegramCommand implements CommandInterface
{
    private CartRepository $cartRepository;
    private UserRepository $userRepository;
    private OrderRepository $orderRepository;
    private LoyaltyRepository $loyaltyRepository;
    private StatusTelegramCommand $statusTelegramCommand;
    public function __construct(private BotHandlerInterface $botHandler,
                                private int                 $chatId,
                                private EntityManagerInterface $entityManager)
    {
        $this->cartRepository = $this->entityManager->getRepository(Cart::class);
        $this->userRepository = $this->entityManager->getRepository(User::class);
        $this->orderRepository = $this->entityManager->getRepository(Order::class);
        $this->loyaltyRepository = $this->entityManager->getRepository(Loyalty::class);

        $this->statusTelegramCommand = new StatusTelegramCommand(
            $this->botHandler,
            $this->chatId,
            $this->entityManager
        );
    }
    public function run(): void
    {
        $user = $this->userRepository->getUserByChatId($this->chatId);
        $cartItems = $this->cartRepository->getCartByUser($user->getId());
        if (!empty($cartItems)) {
            $cartTotalPrice = $this->getCartTotalPrice($cartItems);
            $order = $this->orderRepository->createOrder($user, $cartTotalPrice);
            $this->cartRepository->setOrder($user, $order);

            $this->botHandler->sendMessage(
                $this->chatId,
                'Твой статус: ' . $this->statusTelegramCommand->getStatus() . PHP_EOL
                . 'Заказ №' . $order->getId() . ' успешно создан' . PHP_EOL
                . 'Состав заказа: ' . PHP_EOL
                . implode('', $cartItems)
                . 'на сумму: ' . $this->formatTotalPrice($order->getPrice(), $order->getPriceDiscount()) . PHP_EOL
            );

            $this->updateUserLoyalty($user);
        } else {
            $this->botHandler->sendMessage(
                $this->chatId,
                'Твоя корзина пуста! Сформируй ее командой /cart'
            );
        }
    }

    private function formatTotalPrice(float $price, float $discountPrice): string
    {
        if ($discountPrice < $price) {
            return sprintf('<del>%s руб.</del> %s руб.',
                $price,
                $discountPrice
            );
        }
        return sprintf('%s руб.', $price);
    }

    private function getCartTotalPrice(array $cartItems): float
    {
        $cartTotalPrice = 0;
        foreach($cartItems as $cartItem) {
            $cartTotalPrice += $cartItem->getPrice();
        }
        return $cartTotalPrice;
    }

    private function updateUserLoyalty(User $user): void
    {
        $orderTotalSum = $this->orderRepository->getTotalOrdersSum($user);
        $newLoyalty = $this->loyaltyRepository->findBySum($orderTotalSum);
        if ($user?->getLoyalty()?->getDiscount() != $newLoyalty->getDiscount()) {
            $this->userRepository->setLoyalty($user, $newLoyalty);
        }
    }
}
