<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

readonly class StartTelegramCommand implements CommandInterface
{
    public function __construct(private BotHandlerInterface $botHandler,
                                private int $chatId,
                                private EntityManagerInterface $entityManager)
    {}
    public function run(): void
    {
        $this->botHandler->sendMessage($this->chatId, 'Привет! Я бот программы лояльности'
            . ', перечислю мои команды:' . PHP_EOL
            . '/statuses - Список статусов программы лояльности' . PHP_EOL
            . '/status - Твой статус в программе лояльности' . PHP_EOL
            . '/cart - Автоматическое формирование корзины с товарами' . PHP_EOL
            . '/order - Оформление заказ с учетом скидки в программе лояльности'
        );
    }
}
