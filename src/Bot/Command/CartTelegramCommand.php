<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;
use App\Entity\Cart;
use App\Entity\Loyalty;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\LoyaltyRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class CartTelegramCommand implements CommandInterface
{
    private CartRepository $cartRepository;
    private UserRepository $userRepository;
    private ProductRepository $productRepository;
    public function __construct(private BotHandlerInterface $botHandler,
                                private int                 $chatId,
                                private EntityManagerInterface $entityManager)
    {
        $this->cartRepository = $this->entityManager->getRepository(Cart::class);
        $this->userRepository = $this->entityManager->getRepository(User::class);
        $this->productRepository = $this->entityManager->getRepository(Product::class);
    }
    public function run(): void
    {
        $user = $this->userRepository->getUserByChatId($this->chatId);
        $cartItems = $this->cartRepository->getCartByUser($user->getId());
        if (empty($cartItems)) {
            $products = $this->productRepository->getRandomProducts();
            $cartItems = $this->cartRepository->addProductsToCart($user, $products);
        }

        $this->botHandler->sendMessage(
            $this->chatId,
            'Твоя корзина: ' . PHP_EOL . implode('', $cartItems)
        );
    }
}
