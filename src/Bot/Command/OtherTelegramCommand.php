<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;

class OtherTelegramCommand implements CommandInterface
{
    public function __construct(private BotHandlerInterface $botHandler,
                                private int $chatId)
    {}
    public function run(): void
    {
        $this->botHandler->sendMessage($this->chatId, 'Я не понимаю эту команду. Отправьте /start для получения инструкций.');
    }
}
