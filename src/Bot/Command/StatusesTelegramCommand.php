<?php

namespace App\Bot\Command;

use App\Bot\Handler\BotHandlerInterface;
use App\Entity\Loyalty;
use App\Repository\LoyaltyRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class StatusesTelegramCommand implements CommandInterface
{
    private LoyaltyRepository $loyaltyRepository;
    public function __construct(private BotHandlerInterface $botHandler,
                                private int                 $chatId,
                                private EntityManagerInterface $entityManager)
    {
        $this->loyaltyRepository = $this->entityManager->getRepository(Loyalty::class);
    }
    public function run(): void
    {
        $this->botHandler->sendMessage(
            $this->chatId,
            'Список статусов программы лояльности:' . PHP_EOL . $this->getStatuses()
        );
    }

    private function getStatuses(): string
    {
        return implode('', $this->loyaltyRepository->findAll());
    }
}
