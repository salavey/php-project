<?php

namespace App\Bot\Client;

interface ClientInterface
{
    public function getClient();
}
