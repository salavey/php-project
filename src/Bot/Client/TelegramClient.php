<?php

namespace App\Bot\Client;

use GuzzleHttp\Client;

class TelegramClient implements ClientInterface
{
    private Client $client;

    public function __construct(string $botToken)
    {
        $this->client = new Client([
            'base_uri' => 'https://api.telegram.org/bot' . $botToken . '/',
        ]);
    }
    public function getClient(): Client
    {
        return $this->client;
    }
}
