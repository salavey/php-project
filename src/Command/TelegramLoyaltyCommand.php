<?php

namespace App\Command;

use App\Bot\Client\ClientInterface;
use App\Bot\Client\TelegramClient;
use App\Bot\Command\OtherTelegramCommand;
use App\Bot\Handler\BotHandlerInterface;
use App\Bot\Handler\TelegramHandler;
use App\Repository\LoyaltyRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'telegram:loyalty',
    description: 'Команда считывает телеграм бота системы лояльности',
)]
class TelegramLoyaltyCommand extends Command
{
    private BotHandlerInterface $botHandler;

    public function __construct(private EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $botClient = new TelegramClient($_ENV['TELEGRAM_BOT_TOKEN']);
        $this->botHandler = new TelegramHandler($botClient);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $lastUpdateId = null;

        while (true) {
            try {
                $response = $this->botHandler->getMessages($lastUpdateId ? $lastUpdateId + 1 : null);
                $updates = $this->botHandler->getContents($response);

                foreach ($updates as $update) {
                    $lastUpdateId = $update['update_id'];
                    $botCommand = $update['message']['text'] ?? null;
                    $botCommand = str_replace('/', '', $botCommand);

                    $classCommand = '\\App\\Bot\\Command\\' . ucfirst($botCommand) . 'TelegramCommand';
                    $chatId = $update['message']['chat']['id'];

                    if (class_exists($classCommand)) {
                        (new $classCommand($this->botHandler, $chatId, $this->entityManager))->run();
                    } else {
                        (new OtherTelegramCommand($this->botHandler, $chatId))->run();
                    }
                }
            } catch (GuzzleException $e) {
                new \Exception($e->getMessage());
            }

            sleep(1);
        }

        return Command::SUCCESS;
    }
}
