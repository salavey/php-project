<?php

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Cart>
 */
class CartRepository extends ServiceEntityRepository
{
    public function __construct(private ManagerRegistry $registry)
    {
        parent::__construct($registry, Cart::class);
    }

    public function getCartByUser(int $userId): array
    {
        return $this->findBy(['user' => $userId, 'order' => null]);
    }

    public function addProductsToCart(User $user, array $products): array
    {
        $cartItems = [];
        foreach($products as $product) {
            $cartItem = (new Cart())
                ->setCreatedAt(new \DateTime())
                ->setProduct($product)
                ->setPrice($product->getPrice())
                ->setUser($user)
                ->setQuantity(1);

            $discount = $user?->getLoyalty()?->getDiscount();
            if (!empty($discount)) {
                $cartItem->setPriceDiscount($product->getPrice() * ((100 - $discount) / 100));
            } else {
                $cartItem->setPriceDiscount($product->getPrice());
            }

            $this->registry->getManager()->persist($cartItem);
            $cartItems[] = $cartItem;
        }
        $this->registry->getManager()->flush();
        return $cartItems;
    }

    public function setOrder(User $user, Order $order): void
    {
        $cartItems = $this->getCartByUser($user->getId());
        foreach($cartItems as $cartItem) {
            $cartItem->setOrder($order);
            $this->registry->getManager()->persist($cartItem);
        }
        $this->registry->getManager()->flush();
    }
}
