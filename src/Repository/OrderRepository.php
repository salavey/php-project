<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Order>
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function createOrder(User $user, float $totalPrice): Order
    {
        $order = (new Order())
            ->setCreatedAt(new \DateTime())
            ->setUser($user)
            ->setPrice($totalPrice);

        $discount = $user?->getLoyalty()?->getDiscount();
        if (!empty($discount)) {
            $order->setPriceDiscount($totalPrice * ((100-$discount) / 100));
        } else {
            $order->setPriceDiscount($totalPrice);
        }

        $this->registry->getManager()->persist($order);
        $this->registry->getManager()->flush();
        return $order;
    }

    public function getTotalOrdersSum(User $user): float
    {
        $result = $this->createQueryBuilder('o')
            ->select('SUM(COALESCE(o.priceDiscount, o.price)) AS total_sum')
            ->andWhere('o.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
        return (float) $result ?: 0.0;
    }
}
