<?php

namespace App\Repository;

use App\Entity\Loyalty;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUserByChatId(int $chatId): User
    {
        $user = $this->findOneBy(['chat' => $chatId]);
        if (empty($user)) {
            $user = (new User())->setChat($chatId);
            $this->registry->getManager()->persist($user);
            $this->registry->getManager()->flush();
        }
        return $user;
    }

    public function setLoyalty(User $user, Loyalty $loyalty): void
    {
        $user->setLoyalty($loyalty);
        $this->registry->getManager()->persist($user);
        $this->registry->getManager()->flush();
    }
}
