<?php

namespace App\Repository;

use App\Entity\Loyalty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Loyalty>
 */
class LoyaltyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Loyalty::class);
    }

    public function findBySum(float $sum): ?Loyalty
    {
        return $this->createQueryBuilder('l')
            ->where('l.amountMin <= :sum')
            ->andWhere('l.amountMax >= :sum')
            ->setParameter('sum', (int)$sum)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
