<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getRandomProducts(): array
    {
        $count = rand(2, 3);
        $qb = $this->createQueryBuilder('p')
            ->orderBy('RANDOM()')
            ->setMaxResults($count);
        return $qb->getQuery()->getResult();
    }
}
